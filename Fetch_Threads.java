package io.github.parthacus.greenchan;


import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by parth on 30/10/17.
 */


public class Fetch_Threads {

    //Fetch the Json from 4Chan API
    public static String FetchJSON(String url) throws IOException {
        HttpsURLConnection con = null;
        URL u = new URL(url);
        con = (HttpsURLConnection) u.openConnection();
        con.connect();
        BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line).append("\n");
        }
        br.close();
        return sb.toString();
    }

    //Converts The Fetched Json into a JsonArray
    public static JsonArray FetchJSONObject(String JSON) {
        return new JsonParser().parse(JSON).getAsJsonArray();
    }

    //Fetches the Number of Pages on a Board
    public static int FetchNumberPages(JsonArray jsonArray) {
        return jsonArray.size();
    }

    //Fetches the Number of Threads each Page has
    public static int FetchNumberThreads(JsonArray jsonArray, int Page) {
        return jsonArray.get(Page).getAsJsonObject().get("threads").getAsJsonArray().size();
    }

    //Returns a list of all threads
    public static List<Threads> FetchAllThreads(JsonArray jsonArray, String board) {
        List<Threads> all_threads = new ArrayList<Threads>();

        for (JsonElement page : jsonArray) {
            for (JsonElement thread : page.getAsJsonObject().get("threads").getAsJsonArray()) {
                Threads thisThread = new Threads();
                JsonElement text = thread.getAsJsonObject().get("com");
                if (text != null) {
                    thisThread.text = text.getAsString();
                    Log.e("Text", thisThread.text);
                } else {
                    thisThread.text = "";
                    Log.e("Text", thisThread.text);
                }
                try{
                    thisThread.id = thread.getAsJsonObject().get("no").getAsInt();
                } catch (Exception e){
                    e.printStackTrace();
                }

                thisThread.id = thread.getAsJsonObject().get("no").getAsInt();
                JsonElement title = thread.getAsJsonObject().get("sub");
                if (title != null) {
                    thisThread.title = title.getAsString();
                } else {
                    thisThread.title = "";
                }
                try {
                    thisThread.imageURL = "http://i.4cdn.org/" + board + "/"
                            + thread.getAsJsonObject().get("tim").getAsString()
                            + thread.getAsJsonObject().get("ext").getAsString();

                    Log.e("ImageURL", thisThread.imageURL);

                } catch (Exception e) {
                    thisThread.imageURL = "http://s.4cdn.org/image/filedeleted-res.gif";
                }

                all_threads.add(thisThread);
            }
        }


        return all_threads;
    }

    //Fetches the OP's Post of Each Thread
    public static String FetchTitle(JsonArray jsonArray, int Page, int ThreadNumber) {
        return jsonArray.get(Page).getAsJsonObject()
                .get("threads").getAsJsonArray()
                .get(ThreadNumber).getAsJsonObject()
                .get("com").toString();
    }


}


