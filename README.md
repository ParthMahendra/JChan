# JChan
A Java Wrapper for the 4Chan API designed with Android in mind
```diff
+  >tfw viewing threads doesn't work because project isn't finished
```
#### Kotlin Example Code
###### Load all threads on a board into a list
```kotlin
//Fetches the JSON from the desired board
val Threads_JSON = Fetch_Threads.FetchJSON("https://a.4cdn.org/" + board + "/catalog.json")

//Returns the Array of Threads on a board from JSON
val ThreadsObject = Fetch_Threads.FetchJSONObject(Threads_JSON)

//Returns all the threads as a "Threads" Object from the array on a certain board
val list_Threads = Fetch_Threads.FetchAllThreads(ThreadsObject, board)
```

###### Access a "Threads" Object's variables
```kotlin
for (thread in list_Threads) {
  thread.id       //Thread ID
  thread.text     //OP Text of thread
  thread.title    //Title of the Thread
  thread.imageURL //Image URL of OP's image
}
```
##### Java Example Code coming soon
